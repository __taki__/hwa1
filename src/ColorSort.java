/** Sorting of balls.
 * @since 1.8
 */

import java.util.Arrays;
import java.util.*;

public class ColorSort {

   enum Color {red, green, blue};

   public static void main (String[] param) {
      // for debugging
      Color[] balls = new ColorSort.Color [1000];
      int rCount = 0;
      int gCount = 0;
      int bCount = 0;
      for (int i=0; i < balls.length; i++) {
         double rnd = Math.random();
         if (rnd < 1./3.) {
            balls[i] = ColorSort.Color.red;
            rCount++;
         } else  if (rnd > 2./3.) {
            balls[i] = ColorSort.Color.blue;
            bCount++;
         } else {
            balls[i] = ColorSort.Color.green;
            gCount++;
         }
      }
      reorder(balls);
   }


   public static void reorder (Color[] balls) {
      // TODO!!! Your program here
      if (balls.length < 2) return;

      int rCount = 0;
      int gCount = 0;
      int bCount = 0;

      for (int i=0; i<balls.length; i++) {
         Color temp;
         if (balls[i] == Color.red) {
            temp = balls[rCount];
            balls[rCount] = Color.red;

            if (temp == Color.green) {
               Color temp2 = balls[rCount + gCount];
               balls[rCount + gCount] = Color.green;
               balls[i] = temp2;
            } else if (temp == Color.blue) {
               balls[i] = temp;
            }

            rCount++;
         } else if (balls[i] == Color.green) {
            temp = balls[rCount + gCount];
            balls[rCount + gCount] = Color.green;
            balls[i] = temp;
            gCount++;
         } else {
            balls[rCount + gCount+bCount] = Color.blue;
            bCount++;
         }
      }
   }
}

